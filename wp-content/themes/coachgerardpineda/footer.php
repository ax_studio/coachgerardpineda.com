<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package AX_studio
 */

?>

	</div>

	<footer>
        <nav id="site-navigation" class="main-navigation">
            <?php
            wp_nav_menu( array(
                'theme_location' => 'footer',
                'menu_id'        => 'footer-menu',
            ) );
            ?>
        </nav><!-- #site-navigation -->
	</footer>
</div>

<?php wp_footer(); ?>

</body>
</html>
