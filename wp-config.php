<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'coachgerardpineda' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '3fo6Bk|ED6<){AqUgK6j$ZpxRQ4#jSM^Y+[{c{u)d#mAT0$Hqh`2tr0a})*o:!xM' );
define( 'SECURE_AUTH_KEY',  '(fh:Oisrt N5}Q$)tW+9^^I/%uiDR8&`Bw/j.j3Tu.rh_vTNm:<MWuB/GoU{_{XA' );
define( 'LOGGED_IN_KEY',    'Ak%@/{rdz>6{SW`S|qsLGfdE(8L0.:GDUll0Kg?M*DQv1(=^cu@e=xTt,ef~^8ja' );
define( 'NONCE_KEY',        '=~~];:7Or4O23Cfshi}80pv/94X$v@!w1IC2`O#%>5ac8ZXBzm?M#~oT::4yp_CT' );
define( 'AUTH_SALT',        '5Z6zu9%&&r6AvJV7dv26@TSeWB|cD8,|6eEK9?HLF`(,5*Ac1d[n${-V(/;&]rwu' );
define( 'SECURE_AUTH_SALT', '#+(1}abHDt7Gi5J$,!PC[7u}zWU&-I@]ZI%Hxi x26r9Tv3cnyPc1pT:X&1,68yN' );
define( 'LOGGED_IN_SALT',   'Z xN%wmK,R:4E8}tqE_(y-@|Rhz%f{:A&Q^f2;SM;uu%/gZ4K#qmr!drd8<R7@!%' );
define( 'NONCE_SALT',       'z!x!lr5?Os{s3^rAGX^?OK(!KgS,6vwA]^uRjubp1r=R|nM%)Xzhf-/.!7Z)8iiA' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'coachgerardpineda_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
