#!/usr/bin/env bash

        echo "Ready to deploy, please wait."
        sleep 2


read -p "Enter domain name: " domain

read -p "Please enter your bitbucket username: " bitbucketuser



        if [ -d ./backup-$domain ]; then
            rm -rf backup-$domain
            rm backup-$domain.zip
        fi

        echo "Creating a backup folder"
        sleep 2

        mkdir backup-$domain

        mv $domain ./backup-$domain
        zip -r backup-$domain.zip backup-$domain

        echo "Getting ready to clone, please wait..."
        sleep 2

        git clone --depth 1 https://$bitbucketuser@bitbucket.org/ax_studio/$domain.git

        echo "Restoring upload folder, please wait..."

        if [ -d ./$domain/wp-content/uploads ]; then
            rm -rf ./$domain/wp-content/uploads
        fi

        if [ -d ./$domain/wp-content/plugins ]; then
            rm -rf ./$domain/wp-content/plugins
        fi


        if [ -d ./$domain/wp-content/plugins ]; then
            rm -rf ./$domain/wp-content/uploads
        fi

 cp -r ./backup-$domain/$domain/wp-content/uploads ./$domain/wp-content/uploads
 cp -r ./backup-$domain/$domain/wp-content/plugins ./$domain/wp-content/plugins

        echo "Complete."
        sleep 2
        echo "Deployment successful."

